<?php
require_once('inc/security.php');
require_once('inc/config.php');

require('inc/tasklist/sort.php');
$order = sortTasks();
require('inc/tasklist/filter.php');
$where = filterTasks();
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require('tpl/head.php');	?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">

					<h1 class="page-title">Tâches</h1>
					<?php include('inc/error.php');	?>
					<ul class="tasksort">
						<li class="tasksort-title">Trier par :</li>
						<li>
							<a href="?sort=created&direction=down" <?php sortActive('created', 'down'); ?>><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							Créé par
							<a href="?sort=created&direction=up" <?php sortActive('created', 'up'); ?>><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="?sort=assigned&direction=down" <?php sortActive('assigned', 'down'); ?>><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							Assigné à
							<a href="?sort=assigned&direction=up" <?php sortActive('assigned', 'up'); ?>><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="?sort=status&direction=down" <?php sortActive('status', 'down'); ?>><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							Statut
							<a href="?sort=status&direction=up" <?php sortActive('status', 'up'); ?>><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="?sort=date&direction=down" <?php sortActive('date', 'down'); ?>><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							Date
							<a href="?sort=date&direction=up" <?php sortActive('date', 'up'); ?>><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
						</li>
					</ul>
					<ul class="taskfilter">
						<li class="taskfilter-title">Filtrer par :</li>
						<li><a href="?filter=all" <?php filterActive('all'); ?>>Toutes les tâches</a></li>
						<li><a href="?filter=mine" <?php filterActive('mine'); ?>>Mes tâches</a></li>
						<li><a href="?filter=archived" <?php filterActive('archived'); ?>>Tâches archivées</a></li>
					</ul>
					<ul class="tasklist">
						<?php
						$badge = array(1 => 'success',
													 2 => 'success',
												 	 3 => 'success',
												 	 4 => 'primary',
												 	 5 => 'primary',
												 	 6 => 'warning',
												 	 7 => 'warning',
												 	 8 => 'alert',
												   9 => 'alert');

	          $query = $db -> query('SELECT
																		task.id,
																		description,
																		created_at,
																		due_at,
																		priority,
																		creator.id as creator_id,
																		creator.name as creator_name,
																		creator.picture as creator_picture,
																		finishor.id as finishor_id,
																		finishor.name as finishor_name,
																		finishor.picture as finishor_picture,
																		assignee.id as assignee_id,
																		assignee.name as assignee_name,
																		assignee.picture as assignee_picture
																		FROM task
																		INNER JOIN user as creator on created_by = creator.id
																		LEFT JOIN user as finishor on done_by = finishor.id
																		INNER JOIN user as assignee on assigned_to = assignee.id '.
																		$where.' '.
																		$order);

	          while($data = $query -> fetch()):
	          ?>
						<li class="tasklist-item<?php if(!empty($data['finishor_name'])): ?> tasklist-item-close<?php endif; ?>"<?php if(empty($data['finishor_name'])): ?>onClick="window.location.assign('edit-task.php?id=<?php echo $data['id']; ?>');"<?php endif; ?>>
	            <span class="tasklist-item-id <?php echo $badge[$data['priority']]; ?>">
	              #<?php echo $data['id']; ?>
	            </span>
	            <span class="tasklist-item-description">
	              <span class="tasklist-item-description-text"><?php echo $data['description']; ?></span><br/>
								<span class="tasklist-item-creator">
									Créé par
									<img src="assets/img/profile/<?php echo $data['creator_picture']; ?>" alt="Photo de <?php echo $data['creator_name']; ?>" class="tasklist-item-user-picture"/>
									<?php echo $data['creator_name']; ?>
		            </span>
								<span class="tasklist-item-assignee">
									Assigné à
									<img src="assets/img/profile/<?php echo $data['assignee_picture']; ?>" alt="Photo de <?php echo $data['assignee_name']; ?>" class="tasklist-item-user-picture"/>
									<?php echo $data['assignee_name']; ?>
		            </span>
								<?php if(!empty($data['finishor_name'])): ?>
									<span class="tasklist-item-finishor">
										Fini par
										<img src="assets/img/profile/<?php echo $data['finishor_picture']; ?>" alt="Photo de <?php echo $data['finishor_name']; ?>" class="tasklist-item-user-picture"/>
										<?php echo $data['finishor_name']; ?>
			            </span>
							<?php endif; ?>
	            </span>
							<div class="tasklist-item-right">

		            <span class="tasklist-item-due">
		              <i class="fa fa-clock-o" aria-hidden="true"></i>
									<span class="prettydate"><?php echo $data['due_at']; ?></span>
		            </span>
		            <span class="tasklist-item-actions">
		              <?php if(empty($data['finishor_name'])): ?><a href="done-task.php?id=<?php echo $data['id']; ?>" data-done><?php endif; ?>
		                <i class="fa fa-check" aria-hidden="true"></i>
		              <?php if(empty($data['finishor_name'])): ?></a><?php endif; ?>
		              <?php if(empty($data['finishor_name'])): ?><a href="delete-task.php?id=<?php echo $data['id']; ?>" data-delete><?php endif; ?>
		                <i class="fa fa-times" aria-hidden="true"></i>
		              <?php if(empty($data['finishor_name'])): ?></a><?php endif; ?>
		              <?php if(empty($data['finishor_name'])): ?><a href="edit-task.php?id=<?php echo $data['id']; ?>"><?php endif; ?>
		                <i class="fa fa-pencil" aria-hidden="true"></i>
		              <?php if(empty($data['finishor_name'])): ?></a><?php endif; ?>
		            </span>
							</div>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>

			<div class="add"><a href="add-task.php"><span class="add-text">Ajouter une tâches</span> <i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
		</div>
  </body>
</html>
