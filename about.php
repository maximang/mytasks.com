<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'A propos';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">A propos</h1>
					<p>
						MyTasks est une application Web permettant à un groupe d'utilisateurs de gérer différents tâches de manière commune.
					</p>
					<p>
						Ce projet a été réalisé dans le cadre du cours d'iCommunication que suivent les étudiants en télécommunication de l'<abbr title="Haute école d'ingénierie et d'architecture Fribourg">HEIA-FR</abbr> durant leur première année.
					</p>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
