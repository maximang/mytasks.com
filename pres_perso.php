<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Présentation personnelle';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content page-perso" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Présentation personnelle</h1>
					<header>
						<img src="assets/img/picture.jpg" alt="Maxime Genilloud" title="Maxime Genilloud"/>
						<div class="head">
							<h2>Maxime <strong>Genilloud</strong></h2>
							<div class="right">
								<p>
									079 427 86 58<br />
									<a href="mailto:maxgenilloud@gmail.com">maxgenilloud@gmail.com</a>
								</p>
							</div>
							<div>
								<p>
									Rte de la Croix 34<br/>
									1772 Grolley
								</p>
							</div>
						</div>
						<p class="contact">
							Né le 25 juillet 1995 à Fribourg<br/>
							Originaire de Cressier (FR)
						</p>
					</header>
					<div class="block">
						<h2>Expériences</h2>
						<ul class="timeline">
							<li>
								<span class="date">2016 - 2015</span>
								<span class="description">
									<strong>Software Packaging</strong>, Swisscom SA, Ittigen<br/>
									Création de paquets logiciels pour différents clients
								</span>
							</li>
							<li>
								<span class="date">2015 - 2011</span>
								<strong>Apprentissage de médiamaticien</strong>, Swisscom SA<br/>
								<ul>
									<li>
										<span class="date">2015 - 2013</span>
										<span class="description">
											<strong>Software Packaging</strong>, Ittigen/Ostermundigen<br/>
											Création de paquets logiciels et assurance qualité
										</span>
									</li>
									<li>
										<span class="date">2013 - 2012</span>
										<span class="description">
											<strong>Swisscom Shop</strong>, Avry-sur-Matran<br/>
											<span class="description">Conseil, vente et back office
										</span>
									</li>
									<li>
										<span class="date">2012</span>
										<span class="description">
											<strong>Administration</strong>, Fribourg<br/>
											Soutien administratif aux maîtres d’apprentissage
										</span>
									</li>
									<li>
										<span class="date">2012 - 2011</span>
										<span class="description">
											<strong>Field Services</strong>, Villars-sur-Glâne<br/>
											Support informatique auprès des entreprises clientes
										</span>
									</li>
								</ul>
							</li>
						</ul>
					</div>

					<div class="block">
						<h2>Formation</h2>
						<ul class="timeline">
							<li>
								<span class="date">2015 - 2011</span>
								<span class="description">
									CFC de médiamaticien et maturité commerciale<br/>
									Swisscom SA
								</span>
							</li>
							<li>
								<span class="date">2011 - 2008</span>
								<span class="description">
									Diplôme de fin de scolarité obligatoire<br/>
									Cycle d’orientation de Jolimont, Fribourg
								</span>
							</li>
						</ul>
					</div>

					<div class="block loisirs">
						<h2>Loisir</h2>
						<ul>
							<li>Informatique</li>
							<li>Musique</li>
							<li>Cinéma</li>
							<li>Voyage</li>
						</ul>
					</div>

					<div class="block">
						<h2>Map</h2>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2729.839250694532!2d7.064329515971728!3d46.827166479140644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478e6e1c75878dc9%3A0x96606fb14099f90f!2sRoute+de+la+Croix+34%2C+1772+Grolley!5e0!3m2!1sfr!2sch!4v1498221237518" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
