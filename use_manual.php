<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Manuel d\'utilisation';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Manuel d'utilisation</h1>
					<h2>Introduction</h2>
					<p>
						MyTasks est une application Web permettant à un groupe d'utilisateurs de gérer différents tâches de manière commune.<br/>
						Sur cette page, vous allez trouver différentes informations quant-à l'utilisation de cet outil.
					</p>

					<h2>Connexion</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/login.png" data-lightbox="login" data-title="Page de connexion" class="thumbnail float-left"><img src="assets/img/use_manual/login.png" alt="Page de connexion"/></a>
						Lorsque vous vous rendez sur MyTasks, la première page sur laquelle vous arrivez est la page de connexion.<br/>
						Cette page est composée d'un formulaire de connexion. Il faut le compléter à l'aide de votre adresse e-mail ainsi que de votre mot de passe.<br/>
						Si vous n'avez pas encore de compte, prenez contact avec l'administrateur de la plateforme afin qu'il vous en fournisse un.
					</p>


					<h2>Liste des tâches (accueil)</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/task_list.png" data-lightbox="task_list" data-title="Liste des tâches (accueil)" class="thumbnail float-left"><img src="assets/img/use_manual/task_list.png" alt="Liste des tâches (accueil)"/></a>
						Une fois connecté, vous vous retrouvez directement sur la liste des tâches. Vous avez alors accès à l'ensemble des tâches créées sur la plateforme.<br/>
						En haut, une liste de tris (créé par, assigné à, statut, date) et de filtres (toutes les tâches, mes tâches, tâches archivées) sont disponibles. Ceux-ci vont vous permettre d'utiliser l'application de manière optimale en n'affichant que le contenu qui vous intéresse.<br/>
						Chaque tâche dispose de trois actions : <i class="fa fa-check" aria-hidden="true"></i> marquer la tâche comme réalisée, <i class="fa fa-times" aria-hidden="true"></i> supprimer la tâche et <i class="fa fa-pencil" aria-hidden="true"></i> éditer la tâche.<br/>
						Vous avez également la possibilité de créer une nouvelle tâche grâce au bouton situé en bas (<i class="fa fa-plus-circle" aria-hidden="true"></i>) ou au lien dans le menu.
					</p>

					<h2>Ajouter une tâche</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/add_task.png" data-lightbox="add_task" data-title="Ajouter une tâche" class="thumbnail float-left"><img src="assets/img/use_manual/add_task.png" alt="Ajouter une tâche"/></a>
						Cette page permet simplement d'ajouter une tâche à la liste des tâches.<br/>
						Pour ce faire, il suffit de remplir le formulaire en indiquant : la description de la tâche, sa priorité (sur une échelle de 1 à 9), le délai dans lequel elle doit être réalisée, la personne a qui la tâche est assignée.<br />
						Une fois le formulaire complété, il suffiter de cliquer sur le bouton bleu "Ajouter" afin d'ajouter la tâche. S'il y a des erreurs dans le formulaire, un message d'erreur apparaîtra.
					</p>

					<h2>Editer une tâche</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/edit_task.png" data-lightbox="edit_task" data-title="Editer une tâche" class="thumbnail float-left"><img src="assets/img/use_manual/edit_task.png" alt="Editer une tâche"/></a>
						La page d'édition d'une tâche aborde le même aspect que la page d'ajout.<br />
						En effet, il est possible de modifier la description d'une tâche, sa priorité, son délai et la personne a qui elle est assignée.<br />
						Tout comme pour l'ajout de tâche, en cas d'erreur, un message appaît afin d'aider l'utilisateur à corriger sa saisie.
					</p>

					<h2>Liste des utilisateurs</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/user_list.png" data-lightbox="user_list" data-title="Liste des utilisateurs" class="thumbnail float-left"><img src="assets/img/use_manual/user_list.png" alt="Liste des utilisateurs"/></a>
						La liste d'utiliisateurs permets simplement d'avoir un aperçu général des comptes utilisateurs associés à MyTasks.<br/>
						Le bouton <i class="fa fa-pencil" aria-hidden="true"></i> permet d'éditer un utilisateur, tandis que le bouton <i class="fa fa-times" aria-hidden="true"></i> permet de le supprimer de la base de données.<br />
						Il est également possible d'ajouter une utilisateur grâce au bouton situé en bas (<i class="fa fa-plus-circle" aria-hidden="true"></i>) ou au lien dans le menu.
					</p>

					<h2>Ajouter un utilisateur</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/add_user.png" data-lightbox="add_user" data-title="Ajouter un utilisateur" class="thumbnail float-left"><img src="assets/img/use_manual/add_user.png" alt="Ajouter un utilisateur"/></a>
						Lorsque nécessaire, vous pouvez à tout moment ajouter un utilisateur.<br />
						Pour ce faire, il suffit de compléter le formulaire ci-contre avec les informations suivantes : nom, adresse e-mail, mot de passe et photo de profil (facultatif). Si vous n'ajouter pas de photo de profil, la photo par défaut sera prise.<br />
						En cas d'erreur, un message appaîtra afin de vous aider à corriger votre saisie.
					</p>

					<h2>Editer un utilisateur</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/edit_user.png" data-lightbox="edit_user" data-title="Editer un utilisateur" class="thumbnail float-left"><img src="assets/img/use_manual/edit_user.png" alt="Editer un utilisateur"/></a>
						Le formulaire d'édition d'une utilisateur permet d'éditer ses informations (nom, adresse e-mail, mot de passe et photo de profil).<br />
						Si vous ne souhaitez pas modifier le mot de passe de l'utilisateur, il suffit de laisser le champ mot de passe vide.<br />
						En cas d'erreur, un message appaîtra afin de vous aider à corriger votre saisie.
					</p>

					<h2>Manuel technique</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/tachnical_manual.png" data-lightbox="tachnical_manual" data-title="Manuel technique" class="thumbnail float-left"><img src="assets/img/use_manual/tachnical_manual.png" alt="Manuel technique"/></a>
						Le manuel technique regroupe les différentes informations techniques concernant ce projet.
					</p>

					<h2>Présenation personnelle</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/about.png" data-lightbox="about" data-title="A propos" class="thumbnail float-left"><img src="assets/img/use_manual/about.png" alt="A propos"/></a>
						La page "Présenation personnelle" est accessible via le lien dans le pied de page (Maxime Genilloud).<br />
						Elle fournit des informations générales sur l'auteur de MyTasks sous la forme d'un CV.
					</p>

					<h2>A propos</h2>
					<p class="clearfix">
						<a href="assets/img/use_manual/about.png" data-lightbox="about" data-title="A propos" class="thumbnail float-left"><img src="assets/img/use_manual/about.png" alt="A propos"/></a>
						La page "A propos" fournit des informations générales sur le projet.
					</p>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
