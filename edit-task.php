<?php
require_once('inc/security.php');
if(!isset($_GET['id']) && !is_numeric($_GET['id'])) header('Location:index.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Editer une tâche';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Editer une tâche</h1>
					<?php
					$query = $db -> prepare('SELECT description, due_at, priority, assigned_to FROM task WHERE id = ?');
	        $query -> execute(array($_GET['id']));
	        $data = $query -> fetch();
					?>
					<form method="post" action="edit-task-action.php" class="small-12 medium-6 collumn">
						<?php include('inc/error.php'); ?>
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
            <label for="description">Description</label>
            <textarea name="description" id="description" placeholder="Description de la tâche" rows="6" required><?php echo $data['description']; ?></textarea>
            <label for="priority">Priorité</label>
            <select id="priority" name="priority" required>
							<option value="<?php echo $data['priority']; ?>"><?php echo $data['priority']; ?></option>
              <?php for($i = 1; $i < 10; $i++): ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php endfor; ?>
            </select>
            <label for="due_at">Délai</label>
            <input type="date" name="due_at" id="due_at" value="<?php echo date('Y-m-d', strtotime($data['due_at'])); ?>" required/>
						<label for="assigned_to">Assignée à</label>
						<select id="assigned_to" name="assigned_to" required>
							<?php
							$query = $db -> query('SELECT id, name FROM user');
							$data_users = $query -> fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
							?>
							<option value="<?php echo $data['assigned_to']; ?>"><?php echo $data_users[$data['assigned_to']][0]; ?></option>
							<?php
							foreach($data_users as $key => $d):
								if($key != $data['assigned_to']):
							?>
									<option value="<?php echo $key; ?>"><?php echo $d[0]; ?></option>
							<?php
								endif;
							endforeach;
							?>
            </select>
            <input type="submit" name="submit" value="Modifier" class="button"/>
	        </form>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
