<?php
require('inc/security.php');
require('inc/config.php');

if(isset($_POST['submit'])) {
	$id = $_POST['id'];
  $name = $_POST['name'];
  $email = $_POST['email'];
  $password = $_POST['password'];
	$picture = $_FILES['picture'];

  if(empty($name)) $error[] = 'Le champ "Nom" est vide.';
  if(empty($email)) $error[] = 'Le champ "Adresse e-mail" est vide.';
  elseif(!preg_match('/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD', $email)) $error[] = 'Le champ "Adresse e-mail" est invalide.';
	if(!isset($id))  $error[] = 'ID non renseigné.';
  elseif(!is_numeric($id)) $error[] = 'ID invalide.';
	if(!empty($picture['tmp_name'])) {
		if(!(list($width, $height, $type, $attr) = getimagesize($picture['tmp_name']))) $error[] = 'La photo n\'est pas valide.';
		elseif(!is_uploaded_file($picture['tmp_name'])) $error[] = 'La photo n\'a pas été uploadé correctement !';
		elseif(!empty($picture) && filesize($picture['tmp_name']) > 600000) $error[] = 'La photo pèse trop lourd (max. 600ko).';
	}

  if(isset($error)) {
    $_SESSION['error'] =	$error;

    header('Location:edit-user.php?id='.$id);
    exit();
  }

	$sql = 'UPDATE user SET name =	?, email = ?';
	$data =	array($name, $email);

	if(!empty($password)) {
		require('inc/password.php');
		$sql .= ', password = ?';
	  $data[] = saltAndEncryptPassword($password);
	}

	if(!empty($picture['tmp_name'])) {
		switch($type) {
			case '1':
				$image_create_func = 'imagecreatefromgif';
				$image_save_func = 'imagegif';
				$new_image_ext = 'gif';
				break;

			case '2':
				$image_create_func = 'imagecreatefromjpeg';
				$image_save_func = 'imagejpeg';
				$new_image_ext = 'jpg';
				break;

			case '3':
				$image_create_func = 'imagecreatefrompng';
				$image_save_func = 'imagepng';
				$new_image_ext = 'png';
				break;

			default:
				$_SESSION['error'] =	array('Format de l\'image non supporté (doit être jpg, gif ou png).');
				header('Location:edit-user.php?id='.$id);
				exit();
		}

		$img = $image_create_func($picture['tmp_name']);
		$tmp = imagecreatetruecolor(500, 500);
		imagecopyresampled($tmp, $img, 0, 0, 0, 0, 500, 500, $width, $height);

		$req = $db -> prepare('SELECT picture FROM user WHERE id = ?');
		$req -> execute(array($id));
		$pic = $req -> fetch();

		if($pic['picture'] != 'default.png') unlink('assets/img/profile/'.$pic['picture']);

		$filename =	uniqid().'.'.$new_image_ext;
		$filepath =	'assets/img/profile/'.$filename;
		$image_save_func($tmp, $filepath);

		$sql .= ', picture = ?';
	  $data[] = $filename;

		if($id ==	$_SESSION['user_id'])
			$_SESSION['user_picture'] = $filename;
	}

	$sql .= ' WHERE id = ?';
	$data[] =	$id;

	$query = $db -> prepare($sql);
	$query -> execute($data);
}

header('Location:users.php');
?>
