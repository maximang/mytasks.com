<?php
require('inc/security.php');
require('inc/config.php');

if(isset($_POST['submit'])) {
  $description = $_POST['description'];
  $priority = $_POST['priority'];
  $due_at = $_POST['due_at'];
	$created_by =	$_SESSION['user_id'];
	$assigned_to = $_POST['assigned_to'];

  if(empty($description)) $error[] = 'Le champ "Description" est vide.';
  if(empty($priority)) $error[] = 'Le champ "Priorité" est vide.';
  if(empty($due_at)) $error[] = 'Le champ "Echéance" est vide.';
  if(!in_array($priority, range(1, 9))) $error[] = 'Le champ "Priorité" contient une valeur invalide.';
  if(!strtotime($due_at)) $error[] = 'Le champ "Echéance" contient une valeur invalide.';
	if(empty($assigned_to)) $error[] = '"Assigné à" non renseigné';
	elseif(!is_numeric($assigned_to)) $error[] = '"Assigné à" invalide.';

  if(isset($error)) {
    $_SESSION['error'] =	$error;

    header('Location:add-task.php');
    exit();
  }

  $query = $db -> prepare('INSERT INTO task(description, created_by, due_at, assigned_to, priority) VALUES(?, ?, ?, ?, ?)');
  $query -> execute(array($description, $created_by, $due_at, $assigned_to, $priority));
}

header('Location:index.php');
?>
