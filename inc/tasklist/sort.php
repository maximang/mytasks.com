<?php
$sortList = array('created' => 'created_by', 'assigned' => 'assigned_to', 'status' => 'finishor_name', 'date' => 'due_at');
$directionList = array('up' => 'ASC', 'down' =>	'DESC');
$sort = null; $direction = null;

function sortTasks() {
	global $sortList, $directionList, $sort, $direction;

	if(isset($_GET['sort']) && isset($_GET['direction'])) {
		$sort = $_GET['sort'];
		$direction = $_GET['direction'];
		if(!array_key_exists($sort, $sortList)) $error[] = 'Le type de tri demandé n\'existe pas.';
		if(!array_key_exists($direction, $directionList)) $error[] = 'La direction du tri demandé n\'existe pas.';

		if(isset($error))
	    $_SESSION['error'] =	$error;

		else {
			$_SESSION['sort'] = $sort;
			$_SESSION['direction'] = $direction;
			setcookie('sort', $sort, time()+60*60*24*30);
			setcookie('direction', $direction, time()+60*60*24*30);
			return 'ORDER BY '.$sortList[$sort].' '.$directionList[$direction];
		}
	}

	elseif(isset($_SESSION['sort']) && isset($_SESSION['direction'])) {
		$sort =	$_SESSION['sort'];
		$direction = $_SESSION['direction'];

		if(array_key_exists($sort, $sortList) && array_key_exists($direction, $directionList))
			return 'ORDER BY '.$sortList[$sort].' '.$directionList[$direction];
	}

	elseif(isset($_COOKIE['sort']) && isset($_COOKIE['direction'])) {
		$sort =	$_COOKIE['sort'];
		$direction = $_COOKIE['direction'];

		if(array_key_exists($sort, $sortList) && array_key_exists($direction, $directionList))
			return 'ORDER BY '.$sortList[$sort].' '.$directionList[$direction];
	}
}

function sortActive($sortA, $directionA) {
	global $sort, $direction;

	if(isset($sort) && isset($direction) && $sortA == $sort && $directionA == $direction)
		echo 'class="active"';
}
?>
