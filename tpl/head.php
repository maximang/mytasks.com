	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php if(isset($title)) echo $title.' - '; ?>MyTasks</title>
	<link rel="stylesheet" href="assets/css/app.css">
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="bower_components/lightbox2/src/css/lightbox.css"/>
