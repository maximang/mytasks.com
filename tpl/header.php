		<?php require_once('inc/security.php'); ?>
		<header class="top row expanded">
			<div class="top-bar">
				<div class="top-bar-title">
					<button class="menu-icon hide-for-large" type="button" data-toggle="offCanvas"></button>
					<a href="index.php">MyTasks</a>
				</div>
				<div class="top-bar-right">
					<ul class="dropdown menu" data-dropdown-menu>
						<li>
							<img src="assets/img/profile/<?php echo $_SESSION['user_picture']; ?>" alt="Photo de profil" class="header-picture" />
							<ul class="menu">
								<li><a href="edit-user.php?id=<?php echo $_SESSION['user_id']; ?>"><?php echo $_SESSION['user_name']; ?></a></li>
								<li><a href="logout.php">Déconnexion</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</header>

		<nav class="nav off-canvas-absolute position-left reveal-for-large" id="offCanvas" data-off-canvas>
			<ul class="vertical menu">
				<li><a href="index.php">Liste des tâches</a></li>
				<li><a href="add-task.php">Ajouter une tâche</a></li>
				<li><a href="users.php">Liste des utilisateurs</a></li>
				<li><a href="add-user.php">Ajouter un utilisateur</a></li>
				<li><a href="use_manual.php">Manuel d'utilisation</a></li>
				<li><a href="technical_manual.php">Manuel technique</a></li>
				<li><a href="about.php">A propos</a></li>
			</ul>
		</nav>
