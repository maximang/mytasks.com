<?php
$dbtype = 'mysql';
$dbhost = 'localhost';
$dbname = 'icom';
$dbuser = 'root';
$dbpswd = '';
$dbcharset = 'utf8';

try {
  $db = new PDO($dbtype.':host='.$dbhost.';dbname='.$dbname.';charset='.$dbcharset, $dbuser, $dbpswd);
}
catch(Exception $e) {
  echo($e -> getMessage());
  exit();
}
?>
