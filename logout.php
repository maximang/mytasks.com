<?php
require('inc/security.php');

unset($_SESSION['user_id']);
unset($_SESSION['user_name']);
unset($_SESSION['user_picture']);

header('Location:login.php');
?>
