<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Ajouter un utilisateur';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Ajouter un utilisateur</h1>
					<form method="post" action="add-user-action.php" enctype="multipart/form-data" class="small-12 medium-6 collumn">
						<?php include('inc/error.php'); ?>
						<label for="picture">Photo de profil</label>
						<img src="assets/img/profile/default.png" alt="Photo de profil" class="userform-picture"/><br/>
						<input type="file" name="picture" id="picture"/>
            <label for="name">Nom</label>
            <input type="text" name="name" id="name" placeholder="Nom" required/>
						<label for="email">Adresse e-mail</label>
            <input type="email" name="email" id="email" placeholder="Adresse e-mail" required/>
						<label for="password">Mot de passe</label>
            <input type="password" name="password" id="password"/>
            <input type="submit" name="submit" value="Ajouter" class="button"/>
	        </form>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
