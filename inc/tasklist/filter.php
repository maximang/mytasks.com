<?php
$filterList = array('all' => 1,
							 			'mine' => 'assigned_to = '.$_SESSION['user_id'],
							 			'archived' => 'done_by IS NOT NULL');
$filter = null;

function filterTasks() {
	global $filterList, $filter;

	if(isset($_GET['filter'])) {
		$filter =	$_GET['filter'];

		if(!array_key_exists($filter, $filterList))
			$_SESSION['error'] = 'Le filtre demandé n\'existe pas.';

		else {
			setcookie('filter', $filter, time()+60*60*24*30);
			return 'WHERE '.$filterList[$filter];
		}
	}

	elseif(isset($_SESSION['filter']) && array_key_exists($_SESSION['filter'], $filterList)) {
		$filter =	$_SESSION['filter'];
		return 'WHERE '.$filterList[$filter];
	}

	elseif(isset($_COOKIE['filter']) && array_key_exists($_COOKIE['filter'], $filterList)) {
		$filter =	$_COOKIE['filter'];
		return 'WHERE '.$filterList[$filter];
	}
}

function filterActive($filterA) {
	global $filter;

	if(isset($filter) && $filterA == $filter)
		echo 'class="active"';
}
?>
