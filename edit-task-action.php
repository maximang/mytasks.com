<?php
require('inc/security.php');
require('inc/config.php');

if(isset($_POST['submit'])) {
  $id = $_POST['id'];
  $description = $_POST['description'];
  $priority = $_POST['priority'];
  $due_at = $_POST['due_at'];
	$assigned_to = $_POST['assigned_to'];

  if(empty($description)) $error[] = 'Le champ "Description" est vide.';
  if(empty($priority)) $error[] = 'Le champ "Priorité" est vide.';
  if(empty($due_at)) $error[] = 'Le champ "Echéance" est vide.';
  if(!in_array($priority, range(1, 9))) $error[] = 'Le champ "Priorité" contient une valeur invalide.';
  if(!strtotime($due_at)) $error[] = 'Le champ "Echéance" contient une valeur invalide.';
	if(!isset($id))  $error[] = 'ID non renseigné.';
	if(empty($assigned_to)) $error[] = '"Assigné à" non renseigné';
	elseif(!is_numeric($assigned_to)) $error[] = '"Assigné à" invalide.';

	if(isset($error)) {
    $_SESSION['error'] =	$error;

    header('Location:edit-task.php?id='.$id);
    exit();
  }

  $query = $db -> prepare('UPDATE task SET description = ?, priority = ?, due_at = ?, assigned_to = ? WHERE id = ?');
  $query -> execute(array($description, $priority, $due_at, $assigned_to, $id));
}

header('Location:index.php');
?>
