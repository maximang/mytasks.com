<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Editer un utilisateur';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Editer un utilisateur</h1>
					<?php
					$query = $db -> prepare('SELECT name, email, picture FROM user WHERE id = ?');
	        $query -> execute(array($_GET['id']));
	        $data = $query -> fetch();
					?>
					<form method="post" action="edit-user-action.php" enctype="multipart/form-data" class="small-12 medium-6 collumn">
						<?php include('inc/error.php'); ?>
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
						<label for="picture">Photo de profil</label>
						<img src="assets/img/profile/<?php echo $data['picture']; ?>" alt="Photo de <?php echo $data['name'];	?>" class="userform-picture"/><br/>
						<input type="file" name="picture" id="picture"/>
            <label for="name">Nom</label>
            <input type="text" name="name" id="name" placeholder="Nom" value="<?php echo $data['name'];	?>" required/>
						<label for="email">Adresse e-mail</label>
            <input type="email" name="email" id="email" placeholder="Adresse e-mail" value="<?php echo $data['email'];	?>" required/>
						<label for="password">Mot de passe (laissez le champ vide si vous ne souhaitez pas le modifier)</label>
            <input type="password" name="password" id="password"/>
            <input type="submit" name="submit" value="Modifier" class="button"/>
	        </form>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
