<?php
session_start();

if(isset($_SESSION['user_id']))
	header('Location:index.php');

// TODO: Vérifier l'origine de la requête...

if(isset($_POST['submit'])) {
	$email = $_POST['email'];
	$password = $_POST['password'];

	if(empty($email)) $error[] = 'Le champ "Adresse e-mail" est vide.';
	elseif(!preg_match('/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD', $email)) $error[] = 'Le champ "Adresse e-mail" est invalide.';
	if(empty($password)) $error[] = 'Le champ "Mot de passe" est vide.';

	if(isset($error)) {
		$_SESSION['error'] = $error;

		header('Location:login.php');
		exit();
	}

	require('inc/config.php');
	require('inc/password.php');

	$query = $db -> prepare('SELECT id, name, picture FROM user WHERE email = ? AND password = ?');
	$query -> execute(array($email, saltAndEncryptPassword($password)));
	$result = $query -> fetch();

	if($result != null) {
		$_SESSION['user_id'] = $result['id'];
		$_SESSION['user_name'] = $result['name'];
		$_SESSION['user_picture'] = $result['picture'];
		header('Location:index.php');
		exit();
	}
}

header('Location:login.php');
?>
