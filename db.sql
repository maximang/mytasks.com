-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 26 Juin 2017 à 11:48
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `icom`
--

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `due_at` datetime NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL DEFAULT '1',
  `done_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `done_by`) VALUES
(1, 'Test', '2017-06-07 00:00:00', 1, '2017-06-30 00:00:00', 1, 1, 1),
(2, 'Tâche de test', '2017-06-23 11:32:12', 1, '2017-06-30 00:00:00', 2, 9, 1),
(9, 'Prendre une douche après un week-end au giron', '2017-06-26 11:22:18', 1, '2017-06-25 00:00:00', 1, 8, NULL),
(8, 'Terminer le projet d\'iCommunication', '2017-06-26 11:21:29', 1, '2017-06-26 00:00:00', 1, 9, NULL),
(10, 'Réviser pour l\'examen d\'algorithmique', '2017-06-26 11:22:48', 1, '2017-07-03 00:00:00', 1, 3, NULL),
(11, 'Réviser pour l\'examen de téléinformatique', '2017-06-26 11:23:04', 1, '2017-07-05 00:00:00', 1, 5, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL DEFAULT 'default.png'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `picture`) VALUES
(1, 'Maxime Genilloud', 'maxgenilloud@gmail.com', '1d4d85bf1f3785a5ed8be58fdbd9bd686b741f2419a8834380385c99f355a15c', '594cdf49d1c1a.jpg'),
(2, 'Test', 'test@example.com', '1d4d85bf1f3785a5ed8be58fdbd9bd686b741f2419a8834380385c99f355a15c', '594ce000ac913.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
