$(document).foundation();

$(document).ready(function() {
	$('.prettydate').prettydate({
			default: 'DD-MM-YYYY',
			afterSuffix: 'plus tard',
			beforeSuffix: 'plus tôt',
			autoUpdate: true,
			messages: {
		    second: 'Maintenant',
		    seconds: '%s secondes %s',
		    minute: 'Une minute %s',
		    minutes: '%s minutes %s',
		    hour: 'Une heure %s',
		    hours: '%s heures %s',
		    day: 'Un jour %s',
		    days: '%s jours %s',
		    week: 'Une semaine %s',
		    weeks: '%s semaines %s',
		    month: 'Un mois %s',
		    months: '%s mois %s',
		    year: 'Un an %s',
		    years: '%s ans %s',
		    yesterday: 'Hier',
		    beforeYesterday: 'Avant-hier',
		    tomorrow: 'Demain',
		    afterTomorrow: 'Après-demain'
		}
	});

	$('[data-done]').click(function(e) {
		var elt = $(this);

		$.ajax({
			url: $(elt).attr('href')
		})
		.success(function() {
			$(elt).parents('.tasklist-item').addClass('tasklist-item-close');
		})
		.error(function() {
			alert('Erreur !\nImpossible de marquer la tâche comme effectuée.');
		})

		return false;
	});

	$('[data-delete]').click(function(e) {
		var elt = $(this);

		$.ajax({
			url: $(elt).attr('href')
		})
		.success(function() {
			$(elt).parents('[class*=-item]').fadeOut();
		})
		.error(function() {
			alert('Erreur !\nImpossible de supprimer la tâche.');
		})

		return false;
	});
});
