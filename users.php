<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Liste des utilisateurs';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Liste des utilisateurs</h1>
					<ul class="userlist">
						<?php
	          $query = $db -> query('SELECT id, name, email, picture FROM user');
	          while($data = $query -> fetch()):
	          ?>
						<li class="userlist-item" onClick="window.location.assign('edit-user.php?id=<?php echo $data['id']; ?>');">
	            <span class="userlist-item-id">
	              #<?php echo $data['id']; ?>
	            </span>
	            <span class="userlist-item-name">
								<img src="assets/img/profile/<?php echo $data['picture']; ?>" alt="Photo de <?php echo $data['name']; ?>" class="userlist-item-creator-picture" />
	              <?php echo $data['name']; ?>
	            </span>
							<span class="userlist-item-email">
	              <?php echo $data['email']; ?>
	            </span>
	            <span class="userlist-item-actions">
								<a href="edit-user.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil" aria-hidden="true"></i>
	              </a>
	              <a href="delete-user.php?id=<?php echo $data['id']; ?>" data-delete>
	                <i class="fa fa-times" aria-hidden="true"></i>
	              </a>
	            </span>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>
			</main>

			<?php require('tpl/footer.php');	?>

			<div class="add"><a href="add-user.php"><span class="add-text">Ajouter un utilisateur</span> <i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
		</div>
  </body>
</html>
