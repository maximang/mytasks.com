<?php
require_once('inc/security.php');
require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Manuel technique';
		require('tpl/head.php');
		?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require('tpl/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Manuel technique</h1>

					<h2>Introduction</h2>
					<p>
						Ce manuel regroupe les informations techniques concernant MyTasks.
					</p>

					<h2>Technologies</h2>
					MyTasks est basé sur les technologies suivantes :
					<ul>
						<li class="list-title">Serveurs</li>
						<li>Apache (HTTP)</li>
						<li>PHP</li>
						<li>MySQL</li>
						<li class="list-title">Frameworks et autres</li>
						<li>Foundation</li>
						<li>jQuery (plugins: lightbox2, prettydate)</li>
						<li>Font Awesome</li>
						<li class="list-title">Tools</li>
						<li>Node.js (NPM)</li>
						<li>Bower</li>
						<li>gulp.js</li>
						<li>Git</li>
						<li>Atom</li>
					</ul>

					<h2>Installation</h2>
					<p>
						Pour installer MyTasks, il vous faut tout d'abord cloner le repos Git <code><a href="https://maximang@bitbucket.org/maximang/mytasks.com.git">https://maximang@bitbucket.org/maximang/mytasks.com.git</a></code>.<br />
						Ensuite, il vous suffit de suivre les informations contenues dans le fichier <i>README.md</i> que voici :
					</p>
					<?php
					require('bower_components/parsedown/Parsedown.php');
					$Parsedown = new Parsedown();
					$readme =	$Parsedown->text(file_get_contents('README.md'));
					?>
					<div class="markdown">
						<?php echo $readme; ?>
					</div>
					<p>
						Les informations de connexion à la base de données sont à adapter dans le fichier <code>inc/config.php</code>. La base de données peut être importée à l'aide du fichier <code>db.sql</code>. Les utilisateurs sont listés dans le fichier <code>user.txt</code>.
					</p>
				</div>
			</main>

			<?php require('tpl/footer.php'); ?>
		</div>
  </body>
</html>
