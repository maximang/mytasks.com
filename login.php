<?php
session_start();

if(isset($_SESSION['user_id']))
	header('Location:index.php');

require_once('inc/config.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php
		$title = 'Login';
		require('tpl/head.php');
		?>
  </head>
  <body>
			<main class="row columns small-12 large-4">
					<h1 class="page-title">Login</h1>
					<?php include('inc/error.php'); ?>
					<form method="post" action="logme.php">
						<label for="email">Adresse e-mail</label>
						<input type="email" name="email" id="email" placeholder="Adresse e-mail" required/>

						<label for="password">Mot de passe</label>
						<input type="password" name="password" id="password" placeholder="Mot de passe" required/>

						<input type="submit" name="submit" value="Se connecter" class="button"/>
					</form>
			</main>

			<?php require('inc/script.php');	?>
  </body>
</html>
