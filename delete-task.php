<?php
require('inc/security.php');
if(isset($_GET['id']) && is_numeric($_GET['id']))
{
  require('inc/config.php');
  $query = $db -> prepare('DELETE FROM task WHERE id = ?');
  $query -> execute(array($_GET['id']));
}

elseif(isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	http_response_code(400);
	exit();
}

header('Location:index.php');
?>
